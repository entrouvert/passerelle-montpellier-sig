from django import forms
from django.utils.text import slugify

from .models import MontpellierSig


class MontpellierSigForm(forms.ModelForm):
    class Meta:
        model = MontpellierSig
        exclude = ('slug', 'users')

    def save(self, commit=True):
        if not self.instance.slug:
            self.instance.slug = slugify(self.instance.title)
        return super().save(commit=commit)
