from django.contrib.auth.decorators import login_required
from django.urls import re_path
from passerelle.urls_utils import app_enabled, decorated_includes, required

from .views import *

urlpatterns = [
    re_path(r'^(?P<slug>[\w,-]+)/communes$', CommunesView.as_view(), name='montpellier-sig-communes'),
    re_path(r'^(?P<slug>[\w,-]+)/voies/(?P<insee>\d+)$', VoiesView.as_view(), name='montpellier-sig-voies'),
    re_path(
        r'^(?P<slug>[\w,-]+)/voies/(?P<insee>\d+)/(?P<nom_rue>[\w ]+)/numero$',
        VoiesCommuneView.as_view(),
        name='montpellier-voies-commune',
    ),
    re_path(
        r'^(?P<slug>[\w,-]+)/voiecommune/(?P<nom_rue>[\w ]+)$',
        VoieCommuneView.as_view(),
        name='montpellier-sig-voiecommune',
    ),
    re_path(r'^(?P<slug>[\w,-]+)/reverse$', AdresseView.as_view(), name='montpellier-sig-adresse'),
    re_path(
        r'^(?P<slug>[\w,-]+)/quartier/(?P<insee>\d+)/(?P<nom_rue>[\w ]+)/(?P<numero>\d+)\w*/$',
        DistrictView.as_view(),
        name='montpellier-sig-district',
    ),
    re_path(r'^(?P<slug>[\w,-]+)/viewer/$', ViewerUrlView.as_view(), name='montpellier-sig-viewer'),
]
