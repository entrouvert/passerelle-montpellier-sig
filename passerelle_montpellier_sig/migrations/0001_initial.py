from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MontpellierSig',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'service_url',
                    models.CharField(
                        help_text='SIG Web Service URL', max_length=128, verbose_name='Service URL'
                    ),
                ),
                (
                    'verify_cert',
                    models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
                ),
                ('username', models.CharField(max_length=128, verbose_name='Username', blank=True)),
                ('password', models.CharField(max_length=128, verbose_name='Password', blank=True)),
                (
                    'keystore',
                    models.FileField(
                        help_text='Certificate and private key in PEM format',
                        upload_to=b'monpellier_sig',
                        null=True,
                        verbose_name='Keystore',
                        blank=True,
                    ),
                ),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Montpellier SIG Web Service',
            },
            bases=(models.Model,),
        ),
    ]
