from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_sig', '0002_montpelliersig_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='montpelliersig',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
