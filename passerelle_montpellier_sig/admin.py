from django.contrib import admin

from .models import MontpellierSig


@admin.register(MontpellierSig)
class MontpellierSigAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
