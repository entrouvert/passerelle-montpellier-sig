import logging

import requests
from django.db import models
from django.utils.translation import gettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.jsonresponse import APIError


class MontpellierSig(BaseResource):
    service_url = models.CharField(
        max_length=128, blank=False, verbose_name=_('Service URL'), help_text=_('SIG Web Service URL')
    )
    verify_cert = models.BooleanField(default=True, verbose_name=_('Check HTTPS Certificate validity'))
    username = models.CharField(max_length=128, blank=True, verbose_name=_('Username'))
    password = models.CharField(max_length=128, blank=True, verbose_name=_('Password'))
    keystore = models.FileField(
        upload_to='monpellier_sig',
        null=True,
        blank=True,
        verbose_name=_('Keystore'),
        help_text=_('Certificate and private key in PEM format'),
    )

    category = _('Geographic information system')

    class Meta:
        verbose_name = _('Montpellier SIG Web Service')

    @classmethod
    def get_connector_slug(cls):
        return 'montpellier-sig'

    def sig_request(self, endpoint, **kwargs):
        logger = logging.getLogger(__name__)

        if self.keystore:
            kwargs['cert'] = (self.keystore.path, self.keystore.path)
        if not self.verify_cert:
            kwargs['verify'] = False
        if self.username:
            kwargs['auth'] = (self.username, self.password)
        try:
            resp = requests.get(self.service_url + '/adresse/rest/' + endpoint, **kwargs)
        except requests.exceptions.RequestException as e:
            raise APIError('SIG error: %s' % e)
        if not resp.ok:
            raise APIError('endpoint %r returned status code: %r' % (endpoint, resp.status_code))
        try:
            return resp.json()
        except Exception as e:
            logger.warning('error occured while trying to read json data from %r: %r', endpoint, e)
            return []

    @classmethod
    def get_verbose_name(cls):
        return cls._meta.verbose_name

    def check_status(self):
        self.sig_request('commune')

    @classmethod
    def get_icon_class(cls):
        return 'gis'
