import os

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'

INSTALLED_APPS += ('passerelle_montpellier_sig',)

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DB_ENGINE', 'django.db.backends.sqlite3'),
        'TEST': {
            'NAME': 'passerelle-montpellier-sig-test-%s'
            % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}
