import json

import httmock
from django.test import TestCase
from django.test.client import Client
from passerelle.utils.jsonresponse import APIError

from passerelle_montpellier_sig.models import MontpellierSig
from passerelle_montpellier_sig.views import prefix_cleanup, split_street


class PrefixCleanupTestCase(TestCase):
    def test_prefix_alle(self):
        self.assertEqual(prefix_cleanup('ALL DE LA PAIX'), 'ALLEE DE LA PAIX')

    def test_prefix_viaduc(self):
        self.assertEqual(prefix_cleanup('VIAD ALPHONSE LOUBAT'), 'VIADUC ALPHONSE LOUBAT')

    def test_prefix_voie(self):
        self.assertEqual(prefix_cleanup('VOI DOMITIENNE'), 'VOIE DOMITIENNE')

    def test_prefix_square(self):
        self.assertEqual(prefix_cleanup('SQ SAINT FIACRE'), 'SQUARE SAINT FIACRE')

    def test_prefix_terasse(self):
        self.assertEqual(prefix_cleanup('TSSE TERRASSE DES ALLEES DU BOIS'), 'TERRASSE DES ALLEES DU BOIS')

    def test_prefix_route(self):
        self.assertEqual(prefix_cleanup('RTE DE LODEVE'), 'ROUTE DE LODEVE')

    def test_prefix_rondpoint(self):
        self.assertEqual(prefix_cleanup('RPT RENE CHAR'), 'ROND-POINT RENE CHAR')

    def test_prefix_rue(self):
        self.assertEqual(prefix_cleanup('R WILLIAM ET CATHERINE BOOTH'), 'RUE WILLIAM ET CATHERINE BOOTH')

    def test_prefix_place(self):
        self.assertEqual(prefix_cleanup('PL PABLO PICASSO'), 'PLACE PABLO PICASSO')

    def test_prefix_passage(self):
        self.assertEqual(prefix_cleanup('PAS HANS CHRISTIAN ANDERSEN'), 'PASSAGE HANS CHRISTIAN ANDERSEN')

    def test_prefix_jardin(self):
        self.assertEqual(prefix_cleanup('JARD LES JARDINS DU CORUM'), 'JARDIN LES JARDINS DU CORUM')

    def test_prefix_impasse(self):
        self.assertEqual(prefix_cleanup('IMP VILLEHARDOUIN'), 'IMPASSE VILLEHARDOUIN')

    def test_prefix_esplanade(self):
        self.assertEqual(prefix_cleanup('ESP DE LA MUSIQUE'), 'ESPLANADE DE LA MUSIQUE')

    def test_prefix_chausse(self):
        self.assertEqual(prefix_cleanup('CHE DES GRENADIERS'), 'CHEMIN DES GRENADIERS')

    def test_prefix_boulevard(self):
        self.assertEqual(prefix_cleanup('BD RENOUVIER'), 'BOULEVARD RENOUVIER')

    def test_prefix_avenue(self):
        self.assertEqual(prefix_cleanup('AV RAYMOND DUGRAND'), 'AVENUE RAYMOND DUGRAND')


class ReverseGeolocTest(TestCase):
    def test_get_rue(self):
        self.assertEqual(
            split_street('17 bis R WILLIAM ET CATHERINE BOOTH'), ('17 bis', 'RUE WILLIAM ET CATHERINE BOOTH')
        )

    def test_split_impasse(self):
        self.assertEqual(split_street(' 5 IMP VILLEHARDOUIN'), ('5', 'IMPASSE VILLEHARDOUIN'))

    def test_split_avenue(self):
        self.assertEqual(split_street('42 AV RAYMOND DUGRAND '), ('42', 'AVENUE RAYMOND DUGRAND'))


class ConnectionTest(TestCase):
    def setUp(self):
        self.connector, created = MontpellierSig.objects.get_or_create(
            title='hebe', slug='hebe', description='hebe', service_url='http://hebe.montpellier3m.fr'
        )

    @httmock.all_requests
    def hebe_connection_failed(url, request, *args, **kwargs):
        raise httmock.requests.exceptions.RequestException('connection error')

    @httmock.all_requests
    def hebe_http_error(url, request, *args, **kwargs):
        return httmock.response(500, 'Internal Error')

    def test_connection_fail(self):
        with self.assertRaises(APIError) as e:
            with httmock.HTTMock(self.hebe_http_error):
                self.connector.sig_request('commune')
        self.assertEqual(str(e.exception), "endpoint 'commune' returned status code: 500")
