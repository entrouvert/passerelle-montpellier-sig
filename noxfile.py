import shlex
from pathlib import Path

import nox

nox.options.reuse_venv = True


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        'httmock',
        'https://git.entrouvert.org/entrouvert/passerelle/archive/main.tar.gz',
        'psycopg2',
        'psycopg2-binary',
        'pytest',
        'pytest-cov',
        'pytest-django',
        f'django{django_version}',
        *packages,
    ]

    packages = ['-e', '.'] + packages
    session.install(*packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session
@nox.parametrize('django', ['>=4.2,<4.3'])
def tests(session, django):
    setup_venv(session, django_version=django)

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')

        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov=passerelle_montpellier_sig/',
            '-v',
            f'--junitxml=junit-{django}.xml',
        ]

    args += session.posargs + ['tests/']

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'passerelle.settings',
            'SETUPTOOLS_USE_DISTUTILS': 'stdlib',
            'PASSERELLE_SETTINGS_FILE': 'tests/settings.py',
            'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
        },
    )


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')
